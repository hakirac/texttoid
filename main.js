/*
* Autor: Hakan Özdaşçı, hakirac@yahoo.com
* It gives back a autoincrement idnumber for texte. Every text can have only uniqie id number
* example: textToID("searchedText") or idOlustur("Hakan")
* return: 1 or 2 ... (autoincrement numbers)
* @parameter: text
* @parameter: database (for differences databases)
* @parameter: delete (if you want to remove database)
*/

function textToID(str = "", database = "db", del = "") {

    if (database == "") database = "db";
    let low = require('lowdb')
    let FileSync = require('lowdb/adapters/FileSync')
    let adapter = new FileSync(__dirname + '/' + database + '.json')
    let lwdb = low(adapter)

    // veritabanini silmek istiyorsaniz, adi parametresi ile birlikte "delete" degerini de girin.
    if (del == "delete") {
        lwdb.setState({}).write();
        return (database + " removed. Now " + database + " ID will start 0")
    }
    str = str.trim();
    let db = lwdb.getState();

    if (Object.keys(db).length > 0) { // veritabanı ve veritabanında veri var ise
        let textVarsa = lwdb.get("datas").find({ text: str }).value()

        if (textVarsa) {
            return (textVarsa.id);
        } else { // yoksa yaz ve idyi return et.
            let maxID = lwdb.get("maxID").value(); // en buyuk id yi cekiyoruz.
            maxID = maxID + 1;
            lwdb.get("datas").push({ id: maxID, text: str }).write(); // veritabanina 1 arttirarak yeni id yaziliyor
            lwdb.update('maxID', n => n + 1).write() // veritabanina 1 arttirarak yeni id yaziliyor
            return (maxID);
        }
    } else {
        if (str != "") { // fonksiyona girilen parametre bos degil ise
            lwdb.setState({ datas: [{ id: 1, text: str }], maxID: 1 }).write();
            return (1);
        } else {
            console.error("Lutfen benzersiz ID modulu idOlustur() icin bir metin/deger girin.")
            return undefined;
        }
    }


}

// demo using
//let x = textToID("mehmet")
//console.log(x)

module.exports = textToID