This module convert a text to unique id. Unique ID is autoincrement number. It return always the same number for the same text.
Only for Nodejs

# Installation
>1. $ npm i -g npm
>2. $ npm init
>3. $ npm i --save texttoid

## DEMO 1
```javascript
const textToID = require('texttoid')
```

### For Example:
```javascript
let x = textToID('Hakan Özdaşçı')
console.log(x)
```
>1

```javascript
let x = textToID('Today is Weater Cold')
console.log(x)
```
>2

# If you want to use difference databases
## DEMO 2
```javascript
const textToID = require('texttoid')
```

### For Example:
```javascript
let x = textToID('İlknur Işık', 'documents')
console.log(x)
```
>1

```javascript
let x = textToID('Today is Weater Cold'. 'projects')
console.log(x)
```
>1

```javascript
let x = textToID('Istanbul Metropolitan Municipality'. 'projects')
console.log(x)
```
>2

### It gives back always the same ID for the same textes

# If you want to a new start for IDs (or remove database) 
### For Example 1:
```javascript
let x = textToID('', 'documents', 'delete')
console.log(x)
```
>documents removed.  Now documents ID will start 1. 

### For Example 2:
```javascript
let x = textToID('', 'projects', 'delete')
console.log(x)
```
>projects removed.  Now projects ID will start 1. 

### For Example 3: (Remove default database)
```javascript
let x = textToID('', '', 'delete')
console.log(x)
```
>db removed.  Now db ID will start 1.